package com.example.agenda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Contacto> contactos = new ArrayList<>();
    private EditText txtNombre;
    private EditText txtTelefono;
    private EditText txtTelefono2;
    private EditText txtDireccion;
    private EditText txtNotas;
    private CheckBox cbxFavorito;
    private Contacto saveContact;

    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnListar;
    private Button btnSalir;
    private int index;

    int saveIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = (EditText)findViewById(R.id.txtNombre);
        txtTelefono = (EditText)findViewById(R.id.txtTel1);
        txtTelefono2 = (EditText)findViewById(R.id.txtTel2);
        txtDireccion = (EditText)findViewById(R.id.txtDomicilio);
        txtNotas = (EditText)findViewById(R.id.txtNota);
        cbxFavorito = (CheckBox)findViewById(R.id.chkFavorito);
        btnGuardar = (Button)findViewById(R.id.btnGuardar);
        btnLimpiar = (Button)findViewById(R.id.btnLimpiar);
        btnListar = (Button)findViewById(R.id.btnListar);
        btnSalir = (Button)findViewById(R.id.btnSalir);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txtNombre.getText().toString().equals("") || txtDireccion.getText().toString().equals("") || txtTelefono.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this,R.string.msgError,Toast.LENGTH_SHORT).show();
                }
                else {
                    Contacto contacto = new Contacto();
                    int index = contactos.size();
                    contacto.setNombre(txtNombre.getText().toString());
                    contacto.setTelefono1(txtTelefono.getText().toString());
                    contacto.setTelefono2(txtTelefono2.getText().toString());
                    contacto.setDomicilio(txtDireccion.getText().toString());
                    contacto.setNotas(txtNotas.getText().toString());
                    contacto.setFavorito(cbxFavorito.isChecked());
                    if (saveContact != null) {
                        contacto.setID(saveIndex);
                        for(int x=0; x < contactos.size(); x++) {
                            if ((int) contactos.get(x).getID() == saveIndex) {
                                contactos.set(x, contacto);
                            }
                        }
                        saveContact = null;
                    }
                    else {
                        int idx = index;
                        contacto.setID(idx);
                        contactos.add(contacto);
                        index++;
                    }

                    Toast.makeText(MainActivity.this,R.string.mensaje,Toast.LENGTH_SHORT).show();
                }

            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MainActivity.this,ListActivity.class);
                Bundle objeto = new Bundle();
                objeto.putSerializable("contactos",contactos);
                objeto.putInt("index", index);
                i.putExtras(objeto);
                startActivityForResult(i,0);

            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNombre.setText("");
                txtTelefono.setText("");
                txtTelefono2.setText("");
                txtDireccion.setText("");
                txtNotas.setText("");
                cbxFavorito.setChecked(false);
            }
        });
    }
    public void limpiar(){
        txtNombre.setText("");
        txtTelefono.setText("");
        txtTelefono2.setText("");
        txtDireccion.setText("");
        txtNotas.setText("");
        cbxFavorito.setChecked(false);
        saveContact = null;
    }
    protected void onActivityResult(int requestCode,int resultCode, Intent intent){
        super.onActivityResult(requestCode,resultCode,intent);
        if (intent != null){
            Bundle oBundle = intent.getExtras();
            if (oBundle.getInt("index") > 0) {
                index = oBundle.getInt("index");
            }

            if(oBundle.getSerializable("contacto") instanceof Contacto)
            {
                saveContact = (Contacto) oBundle.getSerializable("contacto");
                saveIndex = oBundle.getInt("index");
                txtNombre.setText(saveContact.getNombre());
                txtTelefono.setText(saveContact.getTelefono1());
                txtTelefono2.setText(saveContact.getTelefono2());
                txtDireccion.setText(saveContact.getDomicilio());
                txtNotas.setText(saveContact.getNotas());
                cbxFavorito.setChecked(saveContact.isFavorito());
            }
            contactos = (ArrayList<Contacto>) oBundle.getSerializable("listaContactos");
            boolean nuevo = oBundle.getBoolean("nuevo");
            if(nuevo){
                limpiar();
            }

        }
    }
}
